#!/bin/sh

docker build . -t layer
docker run layer

docker save layer > layer.tar
docker history layer > history.txt
